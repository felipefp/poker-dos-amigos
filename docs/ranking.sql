SELECT
	users.name AS PARTICIPANTE,
	(
		COALESCE(SUM(campeonato_pontuacoes.value), 0) 
		+ 
		COALESCE((SELECT SUM(1) FROM partida_usuarios v1 INNER JOIN partidas v2 ON v1.partida_id = v2.id INNER JOIN campeonato_pontuacoes v3 ON v1.`position` = v3.`position` WHERE v1.carrasco_id = partida_usuarios.campeonato_usuario_id AND v2.campeonato_id = 1), 0)
	) AS PONTUACAO,
	(SELECT COUNT(1) FROM partida_usuarios a2 WHERE a2.carrasco_id = campeonato_usuarios.id) AS TOTAL_CARRASCO 
FROM
	partida_usuarios
INNER JOIN partidas ON partida_usuarios.partida_id = partidas.id
INNER JOIN campeonato_pontuacoes ON partida_usuarios.`position` = campeonato_pontuacoes.`position`
INNER JOIN campeonato_usuarios ON partida_usuarios.campeonato_usuario_id = campeonato_usuarios.id
INNER JOIN users ON campeonato_usuarios.user_id = users.id
WHERE
	partidas.campeonato_id = 1
GROUP BY partida_usuarios.campeonato_usuario_id
ORDER BY PONTUACAO DESC, TOTAL_CARRASCO DESC;


SELECT COUNT(1) as total, carrasco_id FROM partida_usuarios GROUP BY carrasco_id ORDER BY total dESC ;
