-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: poker
-- ------------------------------------------------------
-- Server version	5.7.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `campeonato_pontuacoes`
--

DROP TABLE IF EXISTS `campeonato_pontuacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campeonato_pontuacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campeonato_id` int(10) unsigned NOT NULL,
  `position` int(11) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campeonato_pontuacoes_campeonato_id_foreign` (`campeonato_id`),
  CONSTRAINT `campeonato_pontuacoes_campeonato_id_foreign` FOREIGN KEY (`campeonato_id`) REFERENCES `campeonatos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campeonato_pontuacoes`
--

LOCK TABLES `campeonato_pontuacoes` WRITE;
/*!40000 ALTER TABLE `campeonato_pontuacoes` DISABLE KEYS */;
INSERT INTO `campeonato_pontuacoes` VALUES (1,1,1,23.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(2,1,2,17.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(3,1,3,12.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(4,1,4,8.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(5,1,5,5.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(6,1,6,3.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(7,1,7,2.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(8,1,8,1.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(9,1,9,1.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(10,1,10,1.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(11,1,11,1.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(12,1,12,1.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(13,1,13,1.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(14,1,14,1.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(15,1,15,1.00,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(16,1,16,1.00,'2018-09-24 13:13:12','2018-09-24 13:13:12');
/*!40000 ALTER TABLE `campeonato_pontuacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campeonato_usuarios`
--

DROP TABLE IF EXISTS `campeonato_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campeonato_usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `campeonato_id` int(10) unsigned NOT NULL,
  `administrator` smallint(6) NOT NULL,
  `stat` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campeonato_usuarios_user_id_foreign` (`user_id`),
  KEY `campeonato_usuarios_campeonato_id_foreign` (`campeonato_id`),
  KEY `campeonato_usuarios_stat_index` (`stat`),
  CONSTRAINT `campeonato_usuarios_campeonato_id_foreign` FOREIGN KEY (`campeonato_id`) REFERENCES `campeonatos` (`id`),
  CONSTRAINT `campeonato_usuarios_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campeonato_usuarios`
--

LOCK TABLES `campeonato_usuarios` WRITE;
/*!40000 ALTER TABLE `campeonato_usuarios` DISABLE KEYS */;
INSERT INTO `campeonato_usuarios` VALUES (1,1,1,1,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(3,3,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(4,4,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(5,5,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(6,6,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(7,7,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(8,8,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(9,9,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(10,10,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(11,11,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(12,12,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(13,13,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(14,14,1,0,1,'2018-09-24 13:13:12','2018-09-24 13:13:12');
/*!40000 ALTER TABLE `campeonato_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campeonatos`
--

DROP TABLE IF EXISTS `campeonatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campeonatos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campeonatos`
--

LOCK TABLES `campeonatos` WRITE;
/*!40000 ALTER TABLE `campeonatos` DISABLE KEYS */;
INSERT INTO `campeonatos` VALUES (1,'2018/2',NULL,'2018-07-01','2018-12-31',10.00,'2018-07-01 00:00:01','2018-07-01 00:00:01');
/*!40000 ALTER TABLE `campeonatos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_06_21_015946_create_campeonatos_table',1),(4,'2018_06_21_020018_create_campeonato_pontuacaos_table',1),(5,'2018_06_21_020038_create_campeonato_usuarios_table',1),(6,'2018_06_21_020136_create_partidas_table',1),(7,'2018_06_21_020201_create_partida_usuarios_table',1),(8,'2018_06_21_025603_create_employees_table',1),(9,'2018_09_24_164709_alter_columns_partida_usuarios_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partida_usuarios`
--

DROP TABLE IF EXISTS `partida_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partida_usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `partida_id` int(10) unsigned NOT NULL,
  `campeonato_usuario_id` int(10) unsigned NOT NULL,
  `paid` smallint(6) DEFAULT '0',
  `position` smallint(6) NOT NULL,
  `carrasco_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `partida_usuarios_partida_id_foreign` (`partida_id`),
  KEY `partida_usuarios_campeonato_usuario_id_foreign` (`campeonato_usuario_id`),
  KEY `partida_usuarios_carrasco_id_foreign` (`carrasco_id`),
  CONSTRAINT `partida_usuarios_campeonato_usuario_id_foreign` FOREIGN KEY (`campeonato_usuario_id`) REFERENCES `campeonato_usuarios` (`id`),
  CONSTRAINT `partida_usuarios_carrasco_id_foreign` FOREIGN KEY (`carrasco_id`) REFERENCES `campeonato_usuarios` (`id`),
  CONSTRAINT `partida_usuarios_partida_id_foreign` FOREIGN KEY (`partida_id`) REFERENCES `partidas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partida_usuarios`
--

LOCK TABLES `partida_usuarios` WRITE;
/*!40000 ALTER TABLE `partida_usuarios` DISABLE KEYS */;
INSERT INTO `partida_usuarios` VALUES (2,1,9,1,1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(3,1,5,1,3,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(4,1,10,1,7,5,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(6,1,6,1,5,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(7,1,1,1,9,5,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(8,1,3,1,2,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(9,1,11,1,8,5,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(10,1,7,1,6,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(11,1,8,1,4,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(17,2,9,1,1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(18,2,5,1,8,6,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(19,2,10,1,6,6,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(21,2,6,1,2,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(23,2,3,1,5,10,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(24,2,11,1,7,8,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(25,2,7,1,4,8,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(26,2,8,1,3,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(32,3,9,1,1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(33,3,5,1,4,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(34,3,10,1,10,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(36,3,6,1,5,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(38,3,3,1,7,5,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(39,3,11,1,3,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(40,3,7,1,8,11,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(41,3,8,1,9,13,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(47,4,9,1,5,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(48,4,5,1,10,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(49,4,10,1,7,11,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(50,4,4,1,6,11,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(51,4,6,1,8,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(52,4,1,1,4,11,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(53,4,3,1,1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(54,4,11,1,2,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(55,4,7,1,3,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(56,4,8,1,9,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(62,5,9,1,1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(63,5,5,1,4,10,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(64,5,10,1,2,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(65,5,4,1,7,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(66,5,6,1,6,7,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(67,5,1,1,10,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(68,5,3,1,5,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(69,5,11,1,8,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(70,5,7,1,2,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(71,5,8,1,9,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(77,6,9,1,3,5,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(78,6,5,1,1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(79,6,10,1,9,4,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(80,6,4,1,4,5,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(81,6,6,1,7,4,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(82,6,1,1,8,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(83,6,3,1,5,5,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(84,6,11,1,10,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(85,6,7,1,6,4,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(86,6,8,1,2,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(92,7,9,1,4,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(94,7,10,1,6,7,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(95,7,4,1,9,7,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(96,7,6,1,3,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(97,7,1,1,1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(98,7,3,1,8,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(99,7,11,1,7,1,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(100,7,7,1,2,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(101,7,8,1,5,6,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(107,8,9,1,3,4,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(109,8,10,1,5,4,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(110,8,4,1,1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(111,8,6,1,2,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(112,8,1,1,7,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(113,8,3,1,8,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(114,8,11,1,4,6,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(116,8,8,1,6,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(122,9,9,1,5,8,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(123,9,5,1,8,6,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(124,9,10,1,7,9,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(125,9,4,1,1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(126,9,6,1,3,4,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(127,9,1,1,10,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(128,9,3,1,6,11,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(129,9,11,1,2,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(130,9,7,1,9,6,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(131,9,8,1,4,11,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(137,10,9,1,3,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(138,10,5,1,1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(139,10,10,1,8,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(140,10,4,1,10,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(141,10,6,1,7,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(142,10,1,1,6,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(143,10,3,1,9,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(144,10,11,1,4,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(145,10,7,1,2,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(146,10,8,1,5,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(153,3,4,1,6,13,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(154,3,12,1,11,13,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(155,3,13,1,2,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(156,4,12,1,10,3,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(157,8,5,1,9,9,'2018-09-24 13:13:12','2018-09-24 13:13:12');
/*!40000 ALTER TABLE `partida_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partidas`
--

DROP TABLE IF EXISTS `partidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partidas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campeonato_id` int(10) unsigned NOT NULL,
  `game_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `partidas_campeonato_id_foreign` (`campeonato_id`),
  CONSTRAINT `partidas_campeonato_id_foreign` FOREIGN KEY (`campeonato_id`) REFERENCES `campeonatos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partidas`
--

LOCK TABLES `partidas` WRITE;
/*!40000 ALTER TABLE `partidas` DISABLE KEYS */;
INSERT INTO `partidas` VALUES (1,1,'2018-07-03','2018-09-24 13:13:12','2018-09-24 13:13:12'),(2,1,'2018-07-10','2018-09-24 13:13:12','2018-09-24 13:13:12'),(3,1,'2018-07-24','2018-09-24 13:13:12','2018-09-24 13:13:12'),(4,1,'2018-07-31','2018-09-24 13:13:12','2018-09-24 13:13:12'),(5,1,'2018-08-07','2018-09-24 13:13:12','2018-09-24 13:13:12'),(6,1,'2018-08-14','2018-09-24 13:13:12','2018-09-24 13:13:12'),(7,1,'2018-08-21','2018-09-24 13:13:12','2018-09-24 13:13:12'),(8,1,'2018-08-28','2018-09-24 13:13:12','2018-09-24 13:13:12'),(9,1,'2018-09-04','2018-09-24 13:13:12','2018-09-24 13:13:12'),(10,1,'2018-09-11','2018-09-24 13:13:12','2018-09-24 13:13:12');
/*!40000 ALTER TABLE `partidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stat` smallint(6) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Felipe','felipe@finhane.com','123','19983396650',1,NULL,'2018-09-19 17:55:23','2018-09-19 17:55:23'),(3,'Mica','mica@finhane.com','123','19983396650',1,NULL,'2018-09-19 17:55:23','2018-09-19 17:55:23'),(4,'Edinho','edinho@finhane.com','123','19983396650',1,NULL,'2018-09-19 17:55:23','2018-09-19 17:55:23'),(5,'Carlão','carlao@finhane.com','123','19983396650',1,NULL,'2018-09-19 17:55:23','2018-09-19 17:55:23'),(6,'Enio','enio@finhane.com','123','19983396650',1,NULL,'2018-09-19 17:55:23','2018-09-19 17:55:23'),(7,'Sergio','sergio@finhane.com','123','19983396650',1,NULL,'2018-09-19 17:55:23','2018-09-19 17:55:23'),(8,'Turati','turati@finhane.com','123','19983396650',1,NULL,'2018-09-19 17:55:23','2018-09-19 17:55:23'),(9,'Bonjorno','bonjorno@finhane.com','123','19983396650',1,NULL,'2018-09-19 17:55:23','2018-09-19 17:55:23'),(10,'Danilo','danilo@finhane.com','123','19983396650',1,NULL,'2018-09-19 17:55:23','2018-09-19 17:55:23'),(11,'Nino','nino@finhane.com','123','19983396650',1,NULL,'2018-09-19 17:55:23','2018-09-19 17:55:23'),(12,'Wilson','wilson@finhane.com','123','19983396650',1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(13,'Borba','borba@ig.com.br','123','19983396650',1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12'),(14,'Renato Xavier','renato@opsweb.com','123','19983396650',1,NULL,'2018-09-24 13:13:12','2018-09-24 13:13:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-24 17:04:02
