<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnsPartidaUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `partida_usuarios` MODIFY `carrasco_id` INTEGER UNSIGNED NULL;');
        DB::statement('ALTER TABLE `partida_usuarios` MODIFY `paid` SMALLINT(6) DEFAULT 0;');
       // Schema::table(
       //     'partida_usuarios',
       //     function (Blueprint $table) {
       //         $table->integer('carrasco_id')->unsigned()->nullable()->change();
       //         //$table->smallInteger('paid')->default(0)->change();
       //     }
       // );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `partida_usuarios` MODIFY `carrasco_id` INTEGER UNSIGNED NOT NULL;');
        DB::statement('ALTER TABLE `partida_usuarios` MODIFY `paid` SMALLINT(6);');
    }
}
